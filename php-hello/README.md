---
page_type: sample
languages:
- php
author:
- Jason
contact:
- jasonwcc@yahoo.com
description: "This sample demonstrates a tiny Hello World PHP app ."
urlFragment: php-hello
---

# PHP Hello World
This sample demonstrates a tiny Hello World PHP app

```
git clone https://gitlab.com/jasonwcc.my/learntodocker
```

On Linux system
```
dnf -y install php
php index.php
```

Build using docker
```
docker build -t php-hello .
docker images
docker run --rm php-hello
Hello PHP World!
Welcome to J web server with PHP
```


Build using docker and host it using apache
```
docker build -t php-hello-apache . -f Dockerfile-apache
docker images
docker run --name php-hello-apache -p 8080:80 -d php-hello-apache
docker logs -l
curl localhost:8081
```

Start a container without building it
```
cd ~/learntophp/hello-world
docker run -d -p 8080:80 --name php-hello-apache2 -p 8081:80 -v -d php:7.2-apache
curl localhost:8081
```

Cleanup
```
docker rm -f php-hello-apache php-hello-apache2
docker rmi -f php-hello-apache  
```




Build using podman
```
podman build -t php-hello .
podman images
podman run -rm php-hello
Hello PHP World!
Welcome to J web server with PHP
```




